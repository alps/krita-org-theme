<!-- start newsletter sign-up (template includes this) -->




    	<div class="row" style="background-color: white; border-bottom: 1px solid #cccccc;border-top: 1px solid #cccccc; margin-top: 3em; padding: 1.5em; margin-bottom: 2em">
    		<div class="col-md-12">
    			<h3 style="margin-top: 0"><?php esc_html_e( 'Krita Mailing List', 'krita-org-theme' )   ?></h3>
    		</div>

    		<div class="col-md-6">
				<p id="emailSignupMessage" style="font-size: 1em; margin-bottom: 1.2em;">
					<?php esc_html_e( 'Be notified with new downloads and upcoming release information. We will not send you any type of Sale or Limited Time Only junk. Just the good stuff!', 'krita-org-theme' )    ?>
				</p>
    		</div>

    		<div class="col-md-6">
				<form id="emailSubmission-form">
					<input  id="emailAddress" type="email" placeholder="<?php esc_html_e( 'Email Address', 'krita-org-theme' )   ?>" class="search-field" style="width: 50%;" />
					<input id="emailSignupButton" type="submit" class="pink-button attached" value="<?php esc_html_e( 'Sign Up', 'krita-org-theme' )   ?>" onclick="_paq.push(['trackEvent', 'Marketing', 'button', 'mailing-list-signup']);"  />
				</form>
    		</div>
    		
    	</div>


    
		<!--<p id="emailSignupTitle" style="color: #555;"><?php esc_html_e( 'Krita Mailing List', 'krita-org-theme' )   ?></p>  -->
		


 
<!-- end newsletter signup grey box -->









<script>

jQuery(document).ready(function($) {

/// newsletter sign up


	$("#emailSignupButton").click(function() {  

		//grab values from form
		var emailAddress = $("input#emailAddress").val();	
		//var message = "none";
		var dataString = "name=" + emailAddress; // + '&message=' + message;  // this would be used as 'data' if you need multiple values
		var mailFormURL =  "<?php echo bloginfo('template_directory') . "/newsletter-email.php"?>";


		//console.log("value for email address: " + dataString);


		if( /(.+)@(.+){2,}\.(.+){2,}/.test(emailAddress) ){
		  // valid email
		 

			$.ajax({  
			  type: "POST",  
			  url: mailFormURL,
			  data: dataString, 
			  success: function(data) {  
				$( "#emailSubmission-form" ).fadeOut();
				$("#emailSignupTitle").html("<?php esc_html_e( 'Almost Done', 'krita-org-theme' )   ?>");
				$("#emailSignupMessage").html("<?php esc_html_e( 'An email will be sent shortly verifying your email address. Once verified, you will start getting notifications.', 'krita-org-theme' )   ?> ");
		
				}  		    
			});  

			return false; 

		}
		else {
		  //invalid email	
		  return false; 	 
		}


	});	// end email sign up click event


});

</script>
