<?php get_header(); ?>

<div class="row" style="margin-top: 5em;">
    <div class="col-md-12 content-container" >                
         <div class="post page">

			<div class="row">
			    <div class="col-md-5" style="border: 0.1em solid rgb(208, 230, 231); background: white;">  
					<h2 style="font-size: 3.2em; text-align: center; "><?php  esc_html_e( 'Nope. Not in here either ...(404)', 'krita-org-theme' ) ?></h2>
 					<p><?php esc_html_e( 'Either the page does not exist any more, or the web address was typed wrong.', 'krita-org-theme' )   ?></p>
                </div>	

<div class="col-md-7">
<img style="box-shadow: none;" src="<?php echo bloginfo('template_directory')?>/images/404-image.gif" />
</div>

		
            </div>
            
            
           
			


            <?php /*
            
                $page = get_page_by_title( '404' );
            
                if ($page)
                {
                    echo $page->post_content;
                }
                else
                {

                    echo ' <p>Default content for now. It would be nice if this could potentially look for a page called 404 that would populate this content instead of being part of the template</p>';



                }    */
            
            ?>
            
       </div>
    </div>   
</div> <!-- end row -->          

<?php get_footer(); ?>
