<?php get_header(); 

        // this Template holds the search results page 


        /* Search Total Count */
        $allsearch = new WP_Query("s=$s&showposts=-1");
        $count = $allsearch->post_count;  
        wp_reset_query();
 
        /* Number of posts per page and page number */
        $num_of_posts = 10;
        $pageNumber = (get_query_var('paged')) ? get_query_var('paged') : 1;
 
        /* Showing the lower post value */
        $n = ($pageNumber-1)*$num_of_posts;
        $n = $n+1;
 
        /* Showing the higher/highest post value */
        $m = $pageNumber * $num_of_posts;
        if($m > $count){
                // if m is bigger than the count var, it sets the highest value equal to the count, this is for the last page of results
                $m = $count;
        }              
?>


    <div class="row content-container" style="margin-top: 5rem">
      
      
      
    	<div class="col-md-12" id="searchResults-nav" >
        
                    <?php if ( have_posts() ) : ?>
					<span id="searchResultsLocation"> <?php esc_html_e( 'Showing', 'krita-org-theme' ); ?> <?php echo $n.'-'.$m.  esc_html__( ' of ', 'krita-org-theme' )    .$count; ?></span>
			<?php endif; ?>
        
            <form method="get" id="searchform" action="https://www.krita.org/">
				<label class="hidden" for="s"></label>
				<label><?php esc_html_e( 'Refine Search', 'krita-org-theme' ); ?></label>
				<input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Search" />
				<input type="submit" id="searchsubmit" value="<?php esc_html_e( 'Go', 'krita-org-theme' ); ?>" />                
			</form>
            

        </div>      
        
    </div>

    

	<div class="row content-container">	

        <div class="col-md-8" >

                <?php if ( have_posts() ) : ?>


                    <?php while ( have_posts() ) : the_post(); ?>
                      <div class="post post-content">		
                        <?php   get_template_part( 'content', 'newsItem')  ?>
                     </div>
                    <?php endwhile; ?>
                     
                    <?php else : ?>
                    <div class="post post-content">		
                        <p><?php esc_html_e( 'No results found', 'krita-org-theme' ); ?></p>
                        
                        
                    </div>
                      
                <?php endif; ?>


                <div class="pagination col-md-12">
                    <?php
                    global $wp_query;

                    $big = 999999999; // need an unlikely integer


                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $wp_query->max_num_pages,
                        'prev_next'          => true,
                        'prev_text'          => __( 'Previous', 'krita-org-theme' ),
                        'next_text'          => __( 'Next', 'krita-org-theme' )
                    ) );
                    ?> 
                </div>
        </div>
    
    
</div>


		  
<?php   get_footer();   ?>
