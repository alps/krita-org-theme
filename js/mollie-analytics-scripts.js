//////////////////
// Take the original JSON data and start transforming it so it is something we can show on the graphs
//////////////////


var paidDonations = jsonData.filter(function (el) {
    return (el[2] === "paid");
});



var donationsByDate = _.countBy(paidDonations, function(item, index) {
  var normalDate = item[0].split(" ")[0];
  return normalDate;
});
//console.log(donationsByDate);


var result = _.reduce(paidDonations, function(prev, curr) {
	var normalDate = curr[0].split(" ")[0];
	
    var found = _.find(prev, function(el) { 
		var elDate = el[0].split(" ")[0];
		return elDate === normalDate; 
	});
	
	if (found) {		
		var newCount = parseFloat(found[4]);
		newCount += parseFloat(curr[4]);
		
		found[4] = newCount.toString();
	} else {
		prev.push(_.clone(curr))
	}
    

    
	return prev;
}, []);
// console.log(result);


// it is possible a day had "0" donations, so we need
// to add some dummy data in that case to make the data easier to see in graph format
// it will mess up calculating months and weeks later if we leave out the dummy data
var lastDateAdded;

// combine the two pieces of data
var numericalDonationsByDate = [];
var index = 0;

for (var item in donationsByDate){

	//console.log("START", index , "-------------------------- current date", result[index][0].split(" ")[0]);


	// reference of the day of the current index
	var currentIndexDate =  convertDateToUTC(new Date(result[index][0].split(" ")[0]) );
	//console.log("Starting vars. last date: ", lastDateAdded);


	//console.log(result[index][0].split(" ")[0], currentIndexDate.toUTCString());

	// fill in empty donation days with dummy data
	 if (index > 0) {

		var nextDay = new Date(lastDateAdded.setDate(lastDateAdded.getDate() + 1));
		//console.log("check performed. next day: ", nextDay);

	 	if (currentIndexDate.getTime() != nextDay.getTime() ) {

	 		while (currentIndexDate.getTime() != nextDay.getTime()) {

				//console.log("next day is not right after last day...adding data");
		 		
		 		// we are missing a day, so construct what the next day would be
		 		var month = nextDay.getUTCMonth() + 1; //months from 1-12
				var day = nextDay.getUTCDate();
				var year = nextDay.getUTCFullYear();
				var newDateString = year + "-" + month + "-" + day;
				//console.log("date to add...", newDateString);
		 		

		 		var dataItem = {
		 		 	'day': newDateString,
		 		 	'peopleThatDonated': 0,
		 		 	'total': 0
		 		};						
		 		numericalDonationsByDate.push(dataItem);

		 		// see if we need to fill in something for the next day
		 		nextDay = new Date(nextDay.setDate(nextDay.getDate() + 1));
	 		}	 		

	 	}

	}




	var dataItem = {
		'day': result[index][0].split(" ")[0],
		'peopleThatDonated': donationsByDate[item],
		'total': result[index][4]
	};
		

	
	numericalDonationsByDate.push(dataItem);

	lastDateAdded = convertDateToUTC(new Date(result[index][0].split(" ")[0]) );
	//console.log(currentIndexDate, lastDateAdded, result[index][0].split(" ")[0]);


	index++;		
}

function convertDateToUTC(date) { 
	return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); 
}



////////////////////////////////////////////////////////
// output data by day. 
//This can be the base for other data that is constructed
//console.log("data condensed into days");
//console.log(numericalDonationsByDate);
//////////////////////////////////////////////////////



// have a view that shows things by month
// take the date range, and get a list of all the weeks

// get the range of data we want to visualize
// use everything for now, but we could change the date ranges later
var startingDateEntry = numericalDonationsByDate[0];
var lastDateEntry = numericalDonationsByDate[numericalDonationsByDate.length];
const startingDayOfTheWeek = 6; // 6 = Sunday, 0 = Monday, 1 = Tues, etc



var dataRange = numericalDonationsByDate.slice() // make a copy of the data to transform
//dataRange.splice(0, 12 ); // filter out data between start and end dates

//console.log("data to analyze");
//console.log(dataRange);




// TODO - the last week is getting cut off... 
// maybe because there were some days with 0 data that aren't being shown in the high level data

function breakEntriesIntoWeeks(dataRange) {  // takes in a date in string format

  	// how many days between start and end? we will use this to loop through and do more calculations
     var weekStorageArray = [];
	 var currentWeekIndex = 0; // which week object will we store things in
	 var currentWeekArray = [];

     for (var i = 0; i < dataRange.length; i++) {
    
     	// if first day, always add to the first array
     	if(i == 0) {
     		currentWeekArray.push(dataRange[i]);     		
     	} else {
	     	// on Sundays, we start putting entries in the next bucket
	     	var dayOfTheWeek = new Date(dataRange[i].day).getDay();

	     	if (dayOfTheWeek == startingDayOfTheWeek || currentWeekArray.length == 7 ) {
	     		currentWeekIndex++;

	     		// put previous entries into the final weekStorayArray
	     		weekStorageArray.push(currentWeekArray);

	     		// clear the current week array to start using it again
	     		currentWeekArray = [];

	     	}

			currentWeekArray.push(dataRange[i]); // add item to array     		
     	}   	

     }

	return weekStorageArray;
}







// variable for summing up properties in an array
var sumPropertyInArray = function(items, prop){
    return items.reduce( function(a, b){
        return parseInt(a) + parseInt(b[prop]);
    }, 0);
};









// this will bring in an array of arrays. 
// Everything in each array needs to be summed up and consolidated to one row
function consolidateForGraph(dataArray) {	
	var consolidatedData = [];
	for (var i = 0; i < dataArray.length; i++) {	
		
		var dayRangeFormatted = dataArray[i][0]["day"] + " -> " + dataArray[i][dataArray[i].length-1]["day"];
		var condensedItem = {
			'day': dayRangeFormatted,
			'peopleThatDonated': sumPropertyInArray(dataArray[i], "peopleThatDonated"),
			'total': sumPropertyInArray(dataArray[i], "total")
		};		

		consolidatedData.push(condensedItem);
		//console.log("total amount by week: ", amount);
	}
	return consolidatedData;
}



var dataByWeeks = breakEntriesIntoWeeks(dataRange);
var preparWeekDataForGraph = consolidateForGraph(dataByWeeks);
//console.log("data broken into weeks");
//console.log(dataByWeeks);

//console.log("condensing into graph data (donations by week chart)");
//console.log(preparWeekDataForGraph);



function breakEntriesIntoMonths(dataRange) { 
	 var currentMonthIndex = 0; // which week object will we store things in
	 var currentMonthArray = [];
	 var monthStorageArray = []; // stores final array of month data

	 // loop through each day
     for (var i = 0; i < dataRange.length; i++) {


     	if(i == 0) {
			// first day, always add to the first array
     		currentMonthArray.push(dataRange[i]);     		
     	} 
     	else {
   	
	     	var dayOfTheWeek = new Date(dataRange[i].day).getDate(); // returns 1-31
			var nextDayOfTheWeek;
	     	if (i == dataRange.length-1 ) { 
				nextDayOfTheWeek = 100; 
	     	} else {
	     		nextDayOfTheWeek = new Date(dataRange[i+1].day).getDate(); // returns 1-31
	     	}			


	     	// check to see if we started again at the first of the month
	     	if (dayOfTheWeek > nextDayOfTheWeek || i == dataRange.length-1 ) {
	     		currentMonthIndex++;
	     		monthStorageArray.push(currentMonthArray);
	     		currentMonthArray = []; // wipe out data for next month
	     	} 

	     	currentMonthArray.push(dataRange[i]); // add item to array  
     	}
     }

     return monthStorageArray;
}

var dataByMonths = breakEntriesIntoMonths(dataRange);
var prepareMonthDataForGraph = consolidateForGraph(dataByMonths);







//////////////////////////////////////////////////////////////

// all the calculations have been done above, so we should be pretty close to just populating the graphs

//////////////////////////////////////////////////////////////


// donations per day
var ctx = document.getElementById("myChart").getContext('2d');
// amount of labels and numbers must be the same
var dateLabelArray = numericalDonationsByDate.map(reg => reg.day);
var donationNumbersArray = numericalDonationsByDate.map(reg => reg.total);
createLineChart(dateLabelArray, 'Donations by DAY through Mollie (in euros)', donationNumbersArray  );



// donations per month
 var ctx = document.getElementById("donationsPerMonth").getContext('2d');
 var dateLabelArray = prepareMonthDataForGraph.map(reg => reg.day);
 var donationNumbersArray = prepareMonthDataForGraph.map(reg => reg.total);
createLineChart(dateLabelArray, 'Donations by MONTH through Mollie (in euros)', donationNumbersArray  );



// line chart for transactions per week ( maybe could re-use the other one by day and just swap data??)
 var ctx = document.getElementById("donationsPerWeek").getContext('2d');
 var dateLabelArray = preparWeekDataForGraph.map(reg => reg.day);
 var donationNumbersArray = preparWeekDataForGraph.map(reg => reg.total);
createLineChart(dateLabelArray, 'Donations by WEEK through Mollie (in euros)', donationNumbersArray  );




// line chart of transactions by day
var ctx = document.getElementById("donationTransactionsChart").getContext('2d');
var dateLabelArray = numericalDonationsByDate.map(reg => reg.day);
var donationNumbersArray = numericalDonationsByDate.map(reg => reg.peopleThatDonated);
createLineChart(dateLabelArray, 'Donation Transactions', donationNumbersArray  );


function createLineChart(labelsArray, dataLabel, datasetData) {
	var lineChartRef = new Chart(ctx, {
	    type: 'line',
	    data: {
	        labels: labelsArray,
	        datasets: [{
	            label: dataLabel,
	            data: datasetData,
	            backgroundColor: [ 'rgba(155, 211, 253, 1.0)' ]
	        }]
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }]
	        },
	         tooltips: {
			    titleFontSize: 20,
			    bodyFontSize: 20,
			    backgroundColor: '#369ce9'
  			}
	    }
	});
}

