
<!-- pages show hierarchy already with page title, so we only need to show title on posts -->

<?php
if ( !is_page()  )
  {
?>
	<h2 class="main-title"><?php  the_title() ?></h2>
<?php
  }
?>






<?php 
 if ( is_single()  )
  {
  	$publishedString = '<p style="font-size: 1em"><em>' .  esc_html__( 'Published ', 'krita-org-theme' ) . '&nbsp;&nbsp;&nbsp;';

    the_date('', $publishedString, '</em></p>');

   // echo "&nbsp;&nbsp;&nbsp;&nbsp;<span style='margin-bottom: 2em'><em>" . esc_html__( 'Published ', 'krita-org-theme' )  .  the_date()   . "</em></span>";
  }

  the_content() 	
?>
