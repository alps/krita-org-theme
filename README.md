# Krita.org Theme file

Krita.org is built using WordPress and PHP. The theme is a custom one that has been modified and tweaked over the the years. It has a number of plugins installed that helps it do various things. 

We turned off comments as we didn't want to deal with that on this stie. It is primarly a marketing site to help people understand what Krita is. We direct people to a number of other sites for community related activities.

## Donations
The website currently uses the payment system "Mollie" for doing transactions and donations. There is a WordPress plugin for this that helps keep track of all the donations that were made. This donation data is stored in WordPress. The donation charts and history pulls this data from this WordPress database table. 

## Changing the homepage
We have occasionally changed the homepage. There is a "Krita fundraiser 2018" template file. You can change the default homepage from the WordPress admin options. 


## News Posts and Artist Interviews
The News posts and artist interviews are just "posts" in WordPress. There is a tag in the post called "Artist Interview" which you can mark it as such. An artist interview mostly does two things. 

* Puts the latest artist interview image thumbnail on the homepage
* Puts the artist interview in the artist interview page which lists the last 20 artist interviews


## Multi-language support
One of the most important plugins installed on this theme is the "PolyLang" plugin. This allows pages and posts to be translated to other languages. 

The theme also supports multiple languages with PO files. These are managed in the "languages" folder.

### Adding new languages
There is a "base pack" of page content that this theme contains. The pages ultimately have to be built in WordPress since they are linked to the English pages. For translators though, Scott has been giving them the base HTML content from each page. This ZIP file is found in the languages folder. 

You will have to configure the new language in WordPress. This will add an option to the drop-down in the header.

### Why is there a "wp-content" folder in here?
For translations to fully work, there is actually some files that need to be placed outside of the themes folder in the wp-content directory. I am not if there is a better place to put these files in this GIT repository, so they are a bit out of place. These files will fix issues with browser related translations. Things like month names, or date formats sometimes need translations for them to display correctly. You cannot do these type of translations in the normal PO files that are in the theme folder. To get to work, place the languages folder inside your wp-content directory.


### Updating translations
The "master" file that controls what can be translated is the POT file that is in the languages folder. For updates, usually the workflow is to first update that with any new strings that might have been added to the theme file. If you are just adding or updating an existing language, you can open the PO file in a program like POEdit. There is an option to update the PO from a POT file. I do that first to make sure the PO file has all the strings that need to be translated. Then you can send the PO file to a translator. They will update the PO file and send the file back. When you get the finished PO file back, open it again in POEdit (or something similar). When you save a PO file, an MO file is generated. MO files is what ultimately what WordPress reads when the site is loaded. When the MO file is updated on the web server, the translations should work on the site at that point.


