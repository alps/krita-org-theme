              
<!--
                <div class="post-excerpt">
            	<div class="excerpt-date">
            			<span style="color: #757474"><?php echo the_time('m/j') ?></span>
            			<span style="font-size: 31px; font-weight: bold"><?php echo the_time('Y') ?></span>
            	</div>
                    <div class="excerpt-content">
                        <a href="<?php echo get_permalink();  ?>"><?php echo the_title()  ?></a>
                        
                        <?php // limit the characters of the excerpt to 300 and add an ellipsis to the end (looks more condensed this way)  ?>
                        <div><?php  echo substr(get_the_excerpt(), 0,300) . '...'; ?></div>
                    </div>
                </div>
-->


        <div class="post-excerpt">
            <div class="excerpt-date">
                <span style="color: #757474"><?php echo date_i18n( the_time(get_option( 'date_format' ) )  ) ?></span>
            </div>
            <div class="excerpt-content">
                <a href="<?php echo get_permalink();  ?>"><?php echo the_title()  ?></a>
                
                <?php // the excerpts will be truncated after 300 characters. The default is too long ?>
                <div><?php   echo substr(get_the_excerpt(), 0,200) . '...';  ?></div>
            </div>
        </div>  
