<?php

global $wpdb;

// $results = $wpdb->get_results( "SELECT * FROM { getRegistrationsTable() }", OBJECT );

// this grabs the current months paid transactions  
$results = $wpdb->get_results( "SELECT created_at, payment_status, amount FROM wp_mollie_forms_payments WHERE payment_status = 'paid' AND MONTH(created_at) = MONTH(CURDATE())
   AND YEAR(created_at) = YEAR(CURDATE()) ", ARRAY_N  );

print "<!-- header monthly payment data -->";
print "<script>var currentMonthDonationsFromMollie = " . json_encode($results) . ";</script>" ;

?>


<style>

.fundraiser-progress-bar-item {
	float: left;
    margin-top: 0.4em;
}

a#fundraiser-bar-container span{
    display: inline-block;
    margin-right: 1rem;
}

a#fundraiser-bar-container span span {
    margin-right: 0;
}


a#fundraiser-bar-container {
    font-family: arial;
    font-size: 0.9em;
    color: #d2e1ec;
    background: #0f2229;
    display: inline-block;
    width: 100%;
    text-decoration: none;
    padding: 0.5em;
}

.fundraiser-progress-bar-message {
	width: 38%; 
	float: left;
}

.fundraiser-progress-bar-message-text {
    text-align: center;
    font-weight: normal;
    padding: 12px 0;
    margin: 0;
    font-size: 1.4em;
}

span.pink-button {
  display: inline-block; 
  padding: 0.2rem 1rem; 
  text-align: center;
}

@media (max-width: 1000px) {

  a#fundraiser-bar-container span{

  }

  #donation-month-container {
  	display: block;
  	width: 100%;
  }

  span.pink-button {
    text-align: center;
    float: none;
  }

  #header-donate-button {
  	display: block;
  	width: 100%;
  	margin: 1rem 0;
  }


}


</style>



<a href="https://krita.org/en/support-us/donations/" id="fundraiser-bar-container">

  <div class="container" style="margin: auto; background: none; box-shadow: none">               
<!--   	<span style="text-decoration: underline">
      <?php echo esc_html_e( 'Help the Krita Foundation!', 'krita-org-theme' ) ?>      
    </span> -->

    <span id="donation-month-container">
    	    <span id="currentDonationPeriod"></span>
    	    <span><?php echo esc_html_e( 'donations', 'krita-org-theme' ) ?> </span>
    </span>


    <span>
      <span id="donationTransactions" style="display: inline; margin-right: 0;"></span> 
      <?php echo esc_html_e( 'people', 'krita-org-theme' ) ?> 
    </span>

    <span>
      &euro; 
      <span id="donationAmountInEuros" style="display: inline; margin-right: 0;"></span>
      <?php echo esc_html_e( 'raised', 'krita-org-theme' ) ?> 
    </span>


  <span id="header-donate-button" class="pink-button"><?php echo esc_html_e( 'Donate now', 'krita-org-theme' ) ?> </span>
  
</div>


<script>
document.getElementById('donationTransactions').innerHTML = currentMonthDonationsFromMollie.length;


var totalDonations = 0;
for (var i = 0; i < currentMonthDonationsFromMollie.length; i++) {
  var amountAsString = currentMonthDonationsFromMollie[i][2];
  totalDonations += parseFloat(amountAsString)
}
document.getElementById('donationAmountInEuros').innerHTML = totalDonations.toFixed(2);



const currentMonthName = new Date().toLocaleString('<?php echo pll_current_language() ?>', { month: 'long' });
var currentYear = new Date().getFullYear();
var amountFinalOutput = currentMonthName + " " + currentYear ;

document.getElementById('currentDonationPeriod').innerHTML = amountFinalOutput

</script>

</a>
