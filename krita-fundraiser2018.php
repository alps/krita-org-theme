<?php
/**
 * Template Name: Fundraiser 2018
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?> 

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>




      <title>
          <?php
          // Print the <title> tag based on what is being viewed.
          global $page, $paged;
      
          wp_title( '|', true, 'right' );
      
          // Add the blog name.
          bloginfo( 'name' );
      
          // Add the blog description for the home/front page.
          $site_description = get_bloginfo( 'description', 'display' );
          if ( $site_description && ( is_home() || is_front_page() ) )  echo " | $site_description";
              
          ?>
      </title>

    <meta charset="<?php echo bloginfo('charset'); ?>"  />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Krita.org">
    <meta name="keywords" content="Krita, drawing, painting, concept, art">
    <meta name="author" content="Krita Foundation">


  	<link href="<?php echo bloginfo('template_directory')?>/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo bloginfo('template_directory')?>/css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
   
   <link href='https://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700' rel='stylesheet' type='text/css'> 

   <?php wp_head(); // this loads jQuery and other necessary libraries ?>

   <style>

h3 {
    font-size: 26px;
    font-weight: normal;
}

h4 {
      font-size: 22px;
    font-weight: normal;
}


.post.page img {
  max-width: 100%;
}

figure {
      border: 1px solid #dadada;
      margin: -2rem;
    margin-bottom: 2rem;
}


figcaption {
    padding: 1rem;
    text-align: center;
    font-size: 28px;
    color: #3babff;
}


    #fundraiser-bg-hero {
      width: 100%;
      height: 950px;




      background-image: url(

        "<?php
          global $wp_query;
          $postid = $wp_query->post->ID;
          echo do_shortcode( get_post_meta($postid, 'background-hero-image', true) ); // allows for shortcodes
          wp_reset_query();
        ?>"

        );   
      background-size: cover;
      background-position: center center;
      position: absolute;
      z-index: 0;
    }

#fundraiser-2018 .main-content {
  background: white;
  padding: 2rem;
  box-shadow: 0px 0px 5px 0px #a5a5a5;
  border-radius: 4px;
}


#fundraiser-2018 .main-content p {
  background: white;
  font-size: 16px;
}


  #fundraiser-2018 .secondary-button, 
  .form-submit input[type=submit] {
    background: #3babff;
    color: white;
    padding: 0.75rem;
    border: 0;
    font-size: 16px;
  }
  #fundraiser-2018 .secondary-button:hover {
    background: #6abfff;;
  }






  .comment-form-comment textarea {
    padding: 1rem;
    border-radius: 4px;
  }

  #fundraiser-2018 .secondary-button img {
    margin: 0;
    padding: 0;
  }


    .container {
      box-shadow: none;
      background: none;
    }

    
@media screen and (max-width: 700px) {
  .fundraiser-sidebar {
      margin: 1rem 0;
      padding: 0;
    }
}






    .content-container {
      padding: 9rem;
      padding-top: 2rem;
    }
    @media screen and (max-width: 1400px) {
      .content-container {
        padding: 4rem; 
      }
    }
    @media screen and (max-width: 700px) {
      .content-container {
        padding: 1rem; 
      }
    }



    .content-container img {
      box-shadow: none;
    }

    .post.page img {
      padding: 0;
      margin: 0;
      margin-bottom: 0.5rem;
    }


.mollie-donation-form {
  background: none;
  box-shadow: none;
}

.fundraiser-goal {
    border: 2px solid #a5f4ff;
    padding: 1.25rem;
    margin-top: 1rem;
    background-color: #fffafc;
    box-shadow: 0px 0px 7px #a0a0a0;
    border-radius: 4px;
}

.fundraiser-goal h3 {
      font-size: 22px;
    font-weight: normal;
}



.fundraiser-form-2018 input,
.fundraiser-form-2018 select {
  padding: 0.5rem;
}


.big-donate-button {
    background: #3babff;
    width: 50%;
    padding: 1.5rem;
    font-size: 35px;
    border: navajowhite;
    border-radius: 13px;
    box-shadow: 3px 3px 5px grey;
    color: white;
}
@media screen and (max-width: 700px) {
.big-donate-button {
   width: 100%;
   font-size: 25px;
  }
}



/* individual styling for the form .
  this will have to change the ID if the form # changes */
.rfmp_currency_8074,
#rfmp_open_amount_8074 input {
    font-size: 68px;
    font-weight: normal;
    padding: 1rem;
}
#rfmp_open_amount_8074 input {
    width: 80%;
}
@media screen and (max-width: 700px) {
.rfmp_currency_8074 {
    font-size: 30px;
  }
}






.mollie-forms-required {
  color: red;
}


::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #e2e2e2;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #e2e2e2;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #e2e2e2;
}
:-moz-placeholder { /* Firefox 18- */
  color: #e2e2e2;
}



.comment-form-comment,
.comment-form-comment label,
.comment-form-comment textarea {
  width: 100%;
}

.comment-author.vcard {
  display: inline-block;
}
.comment-author.vcard img {
  margin: 0;
}

.comment-author .says {
  display: none;
}

.comment-meta.commentmetadata {
      display: inline-block;
    margin-left: 1rem;
    font-size: 14px;
}

li.comment {
    border: 1px solid #cecece;
    padding: 0.5rem;
}

.comment-body .reply {
  font-size: 17px;
}

.comment-body p {
    background: #f1f1f1;
    margin: 0.61rem 0rem;
    padding: 0.5rem;
}
 ol.commentlist {
  padding-left: 0;
 }

 ul.children {
    padding-left: 0;
    list-style: none;
 }

h3#comments {
  display: none;
}


.comment-reply-title {
  display: none;
}

#alternate-currencies span {
  font-size: 1.5rem ;
}

   </style>
</head>

<body id="fundraiser-2018">


<!-- start return to site code banner -->
  <div class="container" style="background: #e4e4e4;">
    <div class="content-container" style="padding: 0.3rem" >

      <div class="row">
        <div class="col-md-12" style="text-align: center"> 
              <?php
                global $wp_query;
                $postid = $wp_query->post->ID;
                echo do_shortcode( get_post_meta($postid, 'return-to-normal-site-message', true) ); // allows for shortcodes
                wp_reset_query();
              ?>
        </div>
      </div>
    </div>
  </div>
<!-- end return to site code banner -->



  <div id="fundraiser-bg-hero"></div>

  <div class="container">
        <div class="content-container" id="content-<?php the_ID(); ?>" >

          <div class="row">

            <div class="col-md-8 main-content"> 
                <?php get_template_part('loop', 'index'); ?>
                <?php comments_template(); ?> 
            </div>

            <div class="col-md-4 fundraiser-sidebar">

              <div class="fundraiser-goal" style="margin-top: 0">

                <?php
                  global $wp_query;
                  $postid = $wp_query->post->ID;
                  echo do_shortcode( get_post_meta($postid, 'fundraiser-right-column', true) ); // allows for shortcodes
                  wp_reset_query();
                ?>

              </div>

              <?php
                global $wp_query;
                $postid = $wp_query->post->ID;
                echo do_shortcode( get_post_meta($postid, 'fundraiser-right-column-bottom', true) ); // allows for shortcodes
                wp_reset_query();
              ?>
              
            </div>

          </div> <!-- end the main content row -->

         

      </div> 
  </div>
  

<!-- start return to site code banner -->
  <div class="container" style="background: #e4e4e4;">
    <div class="content-container" style="padding: 0.3rem" >

      <div class="row">
        <div class="col-md-12" style="text-align: center"> 
              <?php
                global $wp_query;
                $postid = $wp_query->post->ID;
                echo do_shortcode( get_post_meta($postid, 'return-to-normal-site-message', true) ); // allows for shortcodes
                wp_reset_query();
              ?>
        </div>
      </div>
    </div>
  </div>
<!-- end return to site code banner -->









    <!-- used when releases are coming out and we want to build excitement with a countdown. Look at header.php as well -->
    <script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/countdown.min.js"></script>  


<script>
// new Date ( year, month, day, hours, minutes, seconds, milliseconds);
// note that the month is 0 index (ie February is 1)
// custom variable determines the end time. enter value like 2018-10-31

// |countdown.SECONDS |countdown.MINUTES
// later on this timer may be stopped
//window.clearInterval(timerId);
// see countdown.js for more information on options
jQuery(document).ready(function(){
  var releaseDate = new Date( <?php global $wp_query; 
                                   $postid = $wp_query->post->ID; 
                                   echo "'" . get_post_meta($postid, 'fundraiser-end-time', true) . "'"  ; ?>);


  var timerId = countdown( releaseDate, function(ts) {
    //console.log(ts);
        document.getElementById('timeRemainingText').innerHTML =  ts.toHTML();
      },
      countdown.DAYS|countdown.HOURS);
  });
</script>


<script>



jQuery(document).ready(function(){

  var fundraiserElement = document.getElementById("fundraiser-total");
  var initialTotalRaised = fundraiserElement.innerText;
  
  var mainCurreny = initialTotalRaised.split(" ")[0]; // will probably be euro symbol

  var amountRaised = initialTotalRaised.split(" ")[1];
  var mainPartAmount = amountRaised.split(",")[0];
  var decimalAmount = amountRaised.split(",")[1];


  // start formatting. Euro is original price. so all currency converters are converting from that
  //console.log( formatNumberToCurrency("euro", mainPartAmount, decimalAmount ) );
  //console.log( formatNumberToCurrency("dollar", Math.floor(parseInt(mainPartAmount)*1.15), decimalAmount ) );

  // display main currency
  fundraiserElement.innerText = formatNumberToCurrency("euro", mainPartAmount, decimalAmount );


  /* Currency converter stuff isn't working right
  It needs to be able to more accurately separate out all the decimal and thousands separators
  Right now it is getting confused and the conversion is converting the . symbols and parsing it as a 
  decimal separator
  */

  // add to other currencies. This would ID would exist in a custom field, not in this template

  var alternateCurrencyContainer = document.getElementById("alternate-currencies"); 

  if (alternateCurrencyContainer) {
      var currencyConverter = 1.15; // euro * 1.15 == dollars
      var dollarCurrency = document.createElement('span')
      console.log(mainPartAmount);
      dollarCurrency.innerHTML = formatNumberToCurrency("dollar", (parseInt(mainPartAmount)*currencyConverter), decimalAmount );

      alternateCurrencyContainer.appendChild(dollarCurrency);
  }


  var amountPlaceholder = <?php echo "'" . get_post_meta($postid, 'donation-placeholder-amount', true). "'"  ?>;
  
  if (document.getElementsByName("rfmp_amount_8074")) {
      document.getElementsByName("rfmp_amount_8074")[0].setAttribute("placeholder", amountPlaceholder);
  }

});


function formatNumberToCurrency(currencyType, mainAmount, decimalAmount) {

    var formattedMainAmount = "";

    var thousandsSeparator = "";
    var decimalSeparator = "";
    var currencySymbol = "";

    if (currencyType === "euro") {
      thousandsSeparator = ".";
      decimalSeparator = ",";
      currencySymbol = "€ ";
    } if (currencyType === "dollar") {
      thousandsSeparator = ",";
      decimalSeparator = ".";
      currencySymbol = "$ ";
    }

    // for thousands separator

    formattedMainAmount = mainAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, thousandsSeparator);

    return currencySymbol + formattedMainAmount;  // + decimalSeparator + decimalAmount;
}



</script>




    <!-- Matomo (analytics)-->
    <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(['setCookieDomain', '*.krita.org']);
      _paq.push(['setDomains', '*.krita.org']); 
      _paq.push(['setDocumentTitle', document.domain + "/" + document.title]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
      var u="//stats.kde.org/";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', 13]);
      var d=document, g=d.createElement('script'),
    s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true; g.defer=true;
    g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <noscript><p><img src="//stats.kde.org/piwik.php?idsite=13"
    style="border:0;" alt="" /></p></noscript>
    <!-- End Matomo Code -->



<!-- Send donation form to Matomo -->
<script>
    _paq.push(['FormAnalytics::trackForm', document.getElementsByClassName('fundraiser-form-2018')[0] ]);
</script>

 <?php wp_footer(); ?> 
</body>


</html>
