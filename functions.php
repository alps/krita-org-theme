<?php
function my_enqueueScripts() {
    wp_enqueue_script( "jquery" );  // requiref for Bootstrap to function
   //wp_enqueue_script( 'masonry' ); /// grrrr. this is not loading the library. will have to include it manually for now
}
add_action( 'wp_enqueue_scripts', 'my_enqueueScripts' ); // wp_enqueue_scripts action hook to link only on the front-end

 add_theme_support( 'post-thumbnails' );  
 add_theme_support( 'html5', array( 'search-form' ) );
 
 register_nav_menu( 'primary', __( 'Primary Menu', 'krita-org-theme' ) );

add_filter('wp_nav_menu_args', 'prefix_nav_menu_args');

// this function removes the container on the custom navigation objects that are generated
function prefix_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}

// Removes ul class from wp_nav_menu
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );


/**
 * Register our main menu and widgetized areas.
 *
 */
function krita_widgets_init() {

	// need this for the main menu that polylang hooks into
	register_sidebar( array(
		'name'          => 'Main Menu Language',
		'id'            => 'main_menu_lang',
		'before_widget' => '<span id="lang-switcher">',
		'after_widget'  => '</span>',
		'before_title'  => '<span class="rounded">',
		'after_title'   => '</span>',
	) );


	register_sidebar( array(
		'name'          => 'Software Footer Links',
		'id'            => 'software_footer',
		'before_widget' => '<div class="col-md-2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );	

	register_sidebar( array(
		'name'          => 'Education Footer Links',
		'id'            => 'education_footer',
		'before_widget' => '<div class="col-md-2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );	

	register_sidebar( array(
		'name'          => 'Foundation Footer Links',
		'id'            => 'foundation_footer',
		'before_widget' => '<div class="col-md-2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );	


	// homepage left, middle, and right content
	register_sidebar( array(
		'name'          => 'Home page - Left',
		'id'            => 'homepage_left',
		'before_widget' => '<div class="col-md-4 frontpage-block1">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="col-md-12">',
		'after_title'   => '</h2>',
	) );	

	register_sidebar( array(
		'name'          => 'Home page - Middle',
		'id'            => 'homepage_middle',
		'before_widget' => '<div class="col-md-4 frontpage-block2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="col-md-12">',
		'after_title'   => '</h2>',
	) );	

	register_sidebar( array(
		'name'          => 'Home page - Right',
		'id'            => 'homepage_right',
		'before_widget' => '<div class="col-md-4 frontpage-block3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="col-md-12">',
		'after_title'   => '</h2>',
	) );	

}
add_action( 'widgets_init', 'krita_widgets_init' );



// i18n stuff

add_action( 'after_setup_theme', 'setup' );

function setup() {
    load_theme_textdomain( 'krita-org-theme', get_template_directory() . '/languages' );
}



function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
}
add_action( 'init', 'disable_wp_emojicons' );















//  add meta fields to JSON REST API
$args2 = array(
    'type'             => 'string', // Validate and sanitize the meta value as a string.
    'description'    => 'A meta key associated with a string meta value.', // Shown in the schema for the meta key.
    'single'        => false, // Return an array with the type used as the items type. Default: false.
    'show_in_rest'    => true, // Show in the WP REST API response. Default: false.
);
 
register_meta( 'post', 'Version Number', $args2 );
register_meta( 'post', 'Release Notes URL (type \'false\' if none)', $args2 );



// disable RSS feeds (useful when ) 
/*
function fb_disable_feed() {
wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'fb_disable_feed', 1);
add_action('do_feed_atom_comments', 'fb_disable_feed', 1);
*/


// register the polylang strings that need to be localized
//pll_register_string('theme-localize', "Features", 'polylang', false);

 ?>