<?php
            //set up date formatting
            $full_date = get_the_date( $d );                
        ?>

        <div class="post-excerpt">
            <div class="excerpt-date"><?php echo the_time('M') ?><span><?php echo the_time('j') ?></span></div>
            <div class="excerpt-content">
                <a href="<?php echo get_permalink();  ?>"><?php echo the_title()  ?></a>
                <div><?php  echo get_the_excerpt();  ?></div>
            </div>
        </div>