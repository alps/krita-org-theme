<?php
/**
 * Template Name: Krita Post-Download Page
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>
<?php get_header(); ?>


<style>

#post-download-title {
  font-size: 3.8em;
  color: #fff;
  text-align: center;
  padding-bottom: 0.1em;
  font-family: 'Chewy', cursive;
  font-size: 5.3em;
  text-shadow: 3px 6px 7px #1c4060;
} 

.training-links .col-md-6 {
  line-height: 2rem;
}

.training-links .col-md-6 img {
  margin-right: 0.5rem;
  height: 40px;
  width: 50px;
}

.fontpage-panel {
  padding: 0 3rem;
}


</style>









<div class="row" style="  background: #595252; 
  box-shadow: 0px 51px 65px #0a0b0f inset; 
  margin-bottom: 2rem; 
  padding: 5rem 0 2rem; 
  background: url('<?php echo get_template_directory_uri(); ?>/images/decoration/post-download-hero.png'); 
  background-size: cover;
  background-position: center;
  border-bottom: 4px solid #3dacff;">
  <div class="col-md-12">
    <h2 id="post-download-title"><?php esc_html_e( 'Have Fun Painting!', 'krita-org-theme' ); ?></h2>
        
    <div style="text-align: center; margin-bottom: 1.5rem; margin-top: 1rem;">
      
      <span style="background: rgba(0, 0, 0, 0.62); color: white; display: inline-block; margin-bottom: 0.5rem; padding: 0.8rem 1.2rem; border-radius: 4px;">
        <?php esc_html_e( 'Having trouble downloading?', 'krita-org-theme' ); ?> 
        <a style="color: white" href="https://download.kde.org/stable/krita/" target="_blank"><?php esc_html_e( 'Visit the Downloads area.', 'krita-org-theme' ); ?></a>
      </span>

      <div style="color: white">Artwork by <a href="https://www.deviantart.com/sylviaritter/art/Disco-Dingo-786327017" target="_blank" style="color: white"> Sylvia Ritter</a></div>


    </div>
        
  </div>
</div>

<div class="row">
  <div class="col-md-12" >

  </div>

</div>

<div class="row section-row">
  <div class="col-md-7 fontpage-panel">
    <span>

      <!-- language specfic content goes here -->
      <?php get_template_part('loop', 'index'); ?>
      <!-- end language specficic content -->

      <h3><?php esc_html_e( 'Show Us Your Art!', 'krita-org-theme' ); ?></h3>
        <p><?php esc_html_e( 'If you create something you are proud of - we want to see it! We are on a few sites that you can post to', 'krita-org-theme' ); ?><?php esc_html_e( ':', 'krita-org-theme' ); ?></p>
          <div id="socialmedia">                
              <a href="http://krita-free-art-app.deviantart.com/" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-deviantArt']);"><img src="<?php echo get_template_directory_uri(); ?>/images/social-deviantart.png" alt="" /></a>
              <a href="http://krita-foundation.tumblr.com/" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-Tumblr']);"><img src="<?php echo get_template_directory_uri(); ?>/images/social-tumblr.png" alt="" /></a> 
              <a href="https://www.facebook.com/pages/Krita-Foundation/511943358879536" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-facebook']);"><img src="<?php echo get_template_directory_uri(); ?>/images/social-facebook.png" alt="" /></a>
              
              <a href="https://twitter.com/Krita_Painting" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-twitter']);"><img src="<?php echo get_template_directory_uri(); ?>/images/social-twitter.png" alt="" /></a> 
              <a href="https://vk.com/ilovefreeart" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-VK']);"><img src="<?php echo get_template_directory_uri(); ?>/images/social-vk.png" alt="" /></a> 
              <a href="https://www.reddit.com/r/krita/" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-reddit']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-reddit.png" alt="" /></a> 
              <a href="https://mastodon.art/@krita" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-mastodon']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-mastodon.png" alt="" /></a> 
          </div>


      <hr style="margin: 4rem 0"/>


      <h3 style="margin-top: 3rem;"><?php esc_html_e( 'Get Involved ', 'krita-org-theme' ); ?></h3> 
        <p> <?php esc_html_e( 'There is more work than our small team can handle. We are open, welcoming, and supportive.  No resum&eacute; or CV needed!', 'krita-org-theme' ); ?>
          <a href="<?php echo get_permalink( pll_get_post(616) )    ?>" onclick="_paq.push(['trackEvent', 'Marketing', 'link', 'download-volunteer']);"><?php esc_html_e( 'Volunteer with us.', 'krita-org-theme' ); ?> </a>
        </p>



    </span>



  </div>





  <div class="col-md-5 fontpage-panel">
      <span>
        <h3><?php esc_html_e( 'Donations Accepted', 'krita-org-theme' ); ?></h3> 

        <p style="clear:both; margin-bottom: 1rem"><?php esc_html_e( 'It will not just make Krita better for you, but will help thousands of young artists improve without the burden of money or pirating.', 'krita-org-theme' ); ?></p>


        <?php
                  global $wp_query;
                  $postid = $wp_query->post->ID;
                  echo do_shortcode( get_post_meta($postid, 'Mollie Monthly Form', true) ); // allows for shortcodes
                  wp_reset_query();
          ?>

      
      </span>
  </div>


</div>

<?php get_template_part('email-signup-snippet', 'index'); ?>
        
<?php get_footer();  ?>
