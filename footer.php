          

            <div id="footer-decoration" class="row">
              <div class="col-md-12">
              </div>
            </div>

            <footer class="row">




            	<?php dynamic_sidebar( 'software_footer' ); ?>
                <?php dynamic_sidebar( 'education_footer' ); ?>
                <?php dynamic_sidebar( 'foundation_footer' ); ?>

<!--
                <div class="col-md-2">
                    <h5><?php esc_html_e( 'Software ', 'krita-org-theme' ); ?></h5>
                    <ul>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/download/krita-desktop/"><?php esc_html_e( 'Krita Desktop ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/download/krita-gemini/"><?php esc_html_e( 'Krita Gemini ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/download/krita-studio/"><?php esc_html_e( 'Krita Studio ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/features/highlights/"><?php esc_html_e( 'Features ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/features/gallery/"><?php esc_html_e( 'Gallery ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/get-involved/report-a-bug/"><?php esc_html_e( 'Report a Bug ', 'krita-org-theme' ); ?></a></li>
                    </ul>
                </div>
                
                 <div class="col-md-2">
                    <h5><?php esc_html_e( 'Education ', 'krita-org-theme' ); ?></h5>
                    <ul>
                        <li><a href="https://docs.krita.org/KritaFAQ"><?php esc_html_e( 'FAQ ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="https://docs.krita.org/External_Training_and_Tutorials"><?php esc_html_e( 'Tutorials ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="https://docs.krita.org/Main_Page"><?php esc_html_e( 'Documentation ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="https://docs.krita.org/Resources"><?php esc_html_e( 'Resources ', 'krita-org-theme' ); ?></a></li>
                    </ul>
                </div>
                
                <div class="col-md-2">
                   <h5><?php esc_html_e( 'Foundation ', 'krita-org-theme' ); ?></h5>
                    <ul>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/about/krita-foundation/"><?php esc_html_e( 'About Us ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/about/contact/"><?php esc_html_e( 'Contact Us ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/about/press/"><?php esc_html_e( 'Press ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/support-us/donations/"><?php esc_html_e( 'Donate ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="https://userbase.kde.org/What_is_KDE"><?php esc_html_e( 'What is KDE', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/get-involved/overview/"><?php esc_html_e( 'Get Involved ', 'krita-org-theme' ); ?></a></li>
                        <li><a href="<?php echo get_bloginfo('url'); ?>/support-us/shop/"><?php esc_html_e( 'Shop', 'krita-org-theme' ); ?></a></li>
                    </ul>
                </div>

                         -->
                
                <div id="socialmedia" class="col-md-6">                
                    <a class="loginlink" href="https://www.krita.org/wp-admin/" ><?php esc_html_e( 'Admin log in ', 'krita-org-theme' ); ?></a>
                    <a href="http://krita-free-art-app.deviantart.com/" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-deviantArt']);><img src="<?php echo bloginfo('template_directory')?>/images/social-deviantart.png" alt="" /></a>
                    <a href="https://www.facebook.com/pages/Krita-Foundation/511943358879536" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-facebook']);><img src="<?php echo bloginfo('template_directory')?>/images/social-facebook.png" alt="" /></a>
                    
 

                    <a href="https://twitter.com/Krita_Painting" target="_blank" _paq.push(['trackEvent', 'Marketing', 'Social', 'download-twitter']);><img src="<?php echo bloginfo('template_directory')?>/images/social-twitter.png" alt="" /></a>

                    <a href="https://vk.com/ilovefreeart" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-VK']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-vk.png" alt="" /></a> 
                      
                      <a href="https://www.reddit.com/r/krita/" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-reddit']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-reddit.png" alt="" /></a>                   
                      <a href="https://mastodon.art/@krita" target="_blank" onclick="_paq.push(['trackEvent', 'Marketing', 'Social', 'download-mastodon']);"><img src="<?php echo bloginfo('template_directory'); ?>/images/social-mastodon.png" alt="" /></a> 

                </div>
       





        <!-- crazy stuff to get clip-path to work right. it needs to be inline and after the HTML markup -->
          <svg width="0" height="0">
              <defs>
                <clippath id="slim-header-clipPath" clipPathUnits="objectBoundingBox">
                  <polygon points="0 0, 1 0, 1 1, 0 .4" />
                </clippath>
              </defs>
            </svg>

            <svg width="0" height="0">
              <defs>
                <clippath id="header-decoration-clip" clipPathUnits="objectBoundingBox">
                  <polygon points="0 0, 1 0.67, 1 1, 0 .12" />
                </clippath>
              </defs>
            </svg>


            <svg width="0" height="0">
              <defs>
                <clippath id="clip-footer" clipPathUnits="objectBoundingBox">
                  <polygon points="0 0, 1 1, 0 1" />
                </clippath>
              </defs>
            </svg>

            <svg width="0" height="0">
              <defs>
                <clippath id="clip-global-top-banner" clipPathUnits="objectBoundingBox">
                  <polygon points="0 0, 1 0.6, 1 1, 0 1" />
                </clippath>
              </defs>
            </svg>


            <style>
 

            .global-top-banner {
                background: url('https://krita.org/wp-content/uploads/2016/08/pepper-comic-ad.png');
                height: 12em;
                background-size: cover;
                background-position: center;
                border-bottom: 0.5em solid #39a3f2;
                cursor: pointer;
                margin-top: -9.3em;
               -webkit-clip-path: url("#clip-global-top-banner"); 
               clip-path: url("#clip-global-top-banner");
            }

            .global-top-banner.ie-decoration-fix {
                margin-top: -9em;
            }


            /* the full page banner also uses this clip-path */
            #footer-decoration {
               -webkit-clip-path: url("#clip-footer"); 
               clip-path: url("#clip-footer");
               height: 7em;
               background-color: #3babff;
              margin-bottom: -0.05em; /* eliminates a hairline border on the bottom */
              margin-top: 2em;
            }
      
            #slim-background {
                -webkit-clip-path: url("#slim-header-clipPath"); 
               clip-path: url("#slim-header-clipPath");
            }

            #header-decoration {
                height: 11em; /* was 10 */
                margin-top: -9em;
                margin-bottom: 0;
                position: relative;
                -webkit-clip-path: url(#header-decoration-clip);
                clip-path: url(#header-decoration-clip);

                background: #39a3f2;
                background: -moz-linear-gradient(top, #39a3f2 1%, #0078f9 100%);
                background: -webkit-linear-gradient(top, #39a3f2 1%,#0078f9 100%);
                background: linear-gradient(to bottom, #39a3f2 1%,#0078f9 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#39a3f2', endColorstr='#0078f9',GradientType=0 );
            }

            #header-decoration.ie-decoration-fix {
                margin-top: 0;
                height: 1.5em;
                margin-bottom: 5.5em;
                -webkit-clip-path: none;
                clip-path: none;
            }

            #slim-background.ie-decoration-fix {
                    height: 7em;
                -webkit-clip-path: none;
                clip-path: none;
            }

            #footer-decoration.ie-decoration-fix {
              background: none; /* this keeps some nice padding between bottom of content and footer */
              -webkit-clip-path: none;
              clip-path: none;
            }



            @media (max-width: 1600px) {
                 #header-decoration {
                        height: 11em;
                        margin-top: -9em;
                 }

                 #frontpage-slideshow {
                    height: 480px;
                 }
            }


            @media (max-width: 1000px) {

              #acceptCookieButton {
                display: block;
                width: 100%;
                text-align: center;
                padding: 6px;
                margin: 11px 0;
              }


               #frontpage-slideshow {
                  height:211px;
                }

                #header-decoration {
                    height: 5em;
                    margin-top: -4em; 
                }
              
            }



            </style>
        <!-- end crazy stuff to get clip path to work right -->


                
            </footer>
                       
       </div> <!-- end container -->

      <!-- start cookies message -->
       <span id="cookies-message" style="position: fixed; bottom: 0; width: 100%; background: #191c1f; padding: 0.5em; color: white; display: none">
        <?php esc_html_e( 'This website uses cookies to learn about our site usage.', 'krita-org-theme' ); ?>
        <?php esc_html_e( 'If your browser is set to do not track mode we will not use statistics cookies.', 'krita-org-theme' ); ?>         
        <a href="#" id="acceptCookieButton" class="pink-button" style="display:inline-block; margin-left: 1em; float: right; padding: 0.1em 1em; ">
            <?php esc_html_e( 'Got it', 'krita-org-theme' ); ?>
        </a>
       </span>

       <script>
          // helper function for getting specific cookie
          function getCookie(cname) {
              var name = cname + "=";
              var decodedCookie = decodeURIComponent(document.cookie);
              var ca = decodedCookie.split(';');
              for(var i = 0; i <ca.length; i++) {
                  var c = ca[i];
                  while (c.charAt(0) == ' ') {
                      c = c.substring(1);
                  }
                  if (c.indexOf(name) == 0) {
                      return c.substring(name.length, c.length);
                  }
              }
              return "";
          }


          jQuery( document ).ready(function() { 

            jQuery( "#acceptCookieButton" ).click(function(){
              
              // create cookie and hide message. set to expire in 1 year
              var cookieDate = new Date;
              cookieDate.setFullYear(cookieDate.getFullYear( ) + 1);
              document.cookie = "acceptedKritaCookiesPolicy=true; expires=" + cookieDate.toGMTString() + ";" ;

              jQuery("#cookies-message").css('display', 'none' );

            });

            // show message if cookie doesn't exist
            var acceptedCookie = getCookie("acceptedKritaCookiesPolicy");
            if (acceptedCookie != "true") {
              jQuery("#cookies-message").css('display', 'block' );
            }
            
          });

         
          
       </script>
      <!-- end cookies message -->




        
        <script src="<?php echo bloginfo('template_directory')?>/js/bootstrap.min.js"></script>
	    <!-- used when releases are coming out and we want to build excitement with a countdown. Look at header.php as well -->
            <script type="text/javascript" src="<?php echo bloginfo('template_directory')?>/js/countdown.min.js"></script>  


	<!-- Piwik (analytics)-->
	<script type="text/javascript">
	  var _paq = _paq || [];
	  _paq.push(['setCookieDomain', '*.krita.org']);
	  _paq.push(['setDomains', '*.krita.org']); 
	  _paq.push(['setDocumentTitle', document.domain + "/" + document.title]);
	  _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		var u="//stats.kde.org/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 13]);
		var d=document, g=d.createElement('script'),
	s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true;
	g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	  })();
	</script>
	<noscript><p><img src="//stats.kde.org/piwik.php?idsite=13"
	style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->




       <?php wp_footer(); ?> 


         
		<script>

      
		jQuery( document ).ready(function() {

      // Mollie donation form (add default value of 10 euros)
      console.log("about to set value for mollie thing");
      jQuery( "#rfmp_open_amount_7963 input" ).attr("value", "10");




		// individual subscription amount (donation page)
		jQuery( "#individualRecurringAmount" ).keyup(function() {
			if (jQuery( "#individualRecurringAmount" ).val() >= 1) {
	            jQuery( "#individualRecurringHidden" ).attr("value", jQuery( "#individualRecurringAmount" ).val());
	        } else {
	            jQuery( "#individualRecurringHidden" ).attr("value", 1);
	        }
				
		});

/*

            // thank you box download (no minimum)
            jQuery( "#individualOneTimeAmountThankYou" ).keyup(function() {
                //if (jQuery( "#individualOneTimeAmount" ).val() >= 1)                
                jQuery( "#individualOneTimeHidden" ).attr("value", jQuery( "#individualOneTimeAmountThankYou" ).val());

                console.log( jQuery( "#individualOneTimeHidden" ).attr("value"));
            });

            */


			// individual single donation (donation page)
			jQuery( "#individualOneTimeAmount" ).keyup(function() {
				if (jQuery( "#individualOneTimeAmount" ).val() >= 1)	{
                   jQuery( "#individualOneTimeHidden" ).attr("value", jQuery( "#individualOneTimeAmount" ).val()); 
               }	else {
                   jQuery( "#individualOneTimeHidden" ).attr("value", 1); 
               }		
					
			});



			// business single donation (donation page)
			jQuery( "#businessOneTimeAmount" ).keyup(function() {
				if (jQuery( "#businessOneTimeAmount" ).val() >= 50) {
                    jQuery( "#businessOneTimeHidden" ).attr("value", jQuery( "#businessOneTimeAmount" ).val()); 
                } else {
                     jQuery( "#businessOneTimeHidden" ).attr("value", 50);
                }
							
			});


			// business subscription amount (donation page)
			jQuery( "#businessRecurringAmount" ).keyup(function() {
				if (jQuery( "#businessRecurringAmount" ).val() >= 50) {
                    jQuery( "#businessRecurringHidden" ).attr("value", jQuery( "#businessRecurringAmount" ).val());
                } else {
                    jQuery( "#businessRecurringHidden" ).attr("value", 50);
                }
					
			});


		});
    
		</script>




<script>


jQuery(document).ready(function($) {
    
    // if the browser doesn't support clip-path, we need to modify some CSS a bit for the hero
    // it is usually bad practice to sniff for browsers, but IE edge says it supports the clipPath property, but it really doesn't
    // probably since it just copied Firefox's code base when it started
    if (/Edge\/\d./i.test(navigator.userAgent) || 
      navigator.appName == 'Microsoft Internet Explorer' ||  
      !!(navigator.userAgent.match(/Trident/) || 
        navigator.userAgent.match(/rv 11/)) ||  
      $.browser.msie == 1 || 
      navigator.appVersion.match(/Edge/)  || 
      navigator.vendor == "Apple Computer, Inc."

      )
    {

      jQuery("#header-decoration").addClass("ie-decoration-fix");
      jQuery("#slim-background").addClass("ie-decoration-fix");
      jQuery(".global-top-banner").addClass("ie-decoration-fix");  
      jQuery("#footer-decoration").addClass("ie-decoration-fix");  
      jQuery("#frontpage-slideshow").addClass("ie-decoration-fix");
          
      
    } else {
       // console.log("not IE");
    }



});


</script>


    <!-- Mollie donation header -->
    <?php get_template_part( 'back-to-top-button' ); ?> 

    </body>
</html>
