<?php
/**
 * Template Name: Artist Interview
 *
 * @package WordPress
 * @subpackage krita-org-theme
 * @since Twenty Fourteen 1.0
 */

?>


<?php get_header(); ?>

<div class="row">
    <div class="content-container col-md-12" id="content-<?php the_ID(); ?>" >
        <?php get_template_part('loop', 'index'); ?>
    </div>   
</div> <!-- end row -->    





<?php

	if (!is_page())
	{ ?>
	
	<div class="row">
            <div class="col-md-6 content-container">
		<?php get_template_part( 'email-signup-snippet' ); ?>
	    </div>
	</div>

<?php  } ?>


 <div class="row content-container">

            <?php
            
            // create a new WP_Query that gets the latest news (20)
            
            $queryArgs = array(
                'post_type' => 'post',
                'post_status' => 'publish',
                'category_name' => 'Artist Interview',
                'posts_per_page' => 25,
            );
            $posts_Query = new WP_Query($queryArgs);
            
            if (  $posts_Query->have_posts() ) 
            {
                while ( $posts_Query->have_posts() ) 
                {
                    $posts_Query->the_post();

                    //set up date formatting
                    $full_date = get_the_date( $d );                
                ?>

                  
                    
                    <?php if (has_post_thumbnail( $post->ID ) ): ?>
                    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
                    
                    <div class="col-md-4 interview-container">   
                         <a href="<?php echo get_permalink();  ?>">
                            <div class="interview-featuredImage" style="background-image: url('<?php echo $image[0]; ?>')"></div>
                            <div class="interview-name"> <?php echo the_title()  ?></div>
                            
                         </a>
                          </div>       
                     
                        
                    <?php endif; ?>

          
                    
                    
                 <?php   
                }
            }
            ?>
</div>    
         
        
<?php get_footer(); ?>
